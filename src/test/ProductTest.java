package test;


import main.entidades.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProductTest {

    private Product product;

    @BeforeEach
    public void inicializar(){
        this.product = new Product("Computador", 2000.00, 5);
    }

    @Test
    public void deveRetornarValorTotalNoEstoque(){
        double valorTotal = product.totalValueInStock();
        assertEquals(10000, valorTotal);
    }

    @Test
    public void deveAdicionarProdutosAoEstoque(){
        product.addProducts(4);
        assertEquals(9, product.getQuantity());
    }


    @Test
    public void removerProdutosDoEstoque(){
        product.removeProduct(3);
        assertEquals(2, product.getQuantity());
    }
}
